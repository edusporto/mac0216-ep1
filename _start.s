extern main

GLOBAL _start

SECTION .text

_start:
    xor ebp, ebp        ; zera ebp
    pop esi             ; esi = argc
    mov ecx, esp        ; ecx = argv
    and esp, 0xfffffff0 ; alinha o stack pointer para otimização

    push eax
    push eax
    push eax
    push eax
    push eax
    push eax            ; lixo para alinhamento
    push ecx            ; parametro argv
    push esi            ; parametro argc

    call main

    mov ebx, eax        ; ebx recebe o retorno de main, guardado em eax
    mov eax, 1          ; exit, syscall 1
    int 0x80            ; chama o kernel