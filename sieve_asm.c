#include <stdio.h>
#include <stdlib.h>

extern int is_prime(int n);

int main(int argc, char **argv) {
    if (argc != 2) {
        // não colocou primo ou
        // colocou mais que 1 parametro
        printf("Usage: %s <NUMBER>\n", argv[0]);

        return 1;
    }

    if (is_prime(atoi(argv[1]))) {
        printf("1\n");
    } else {
        printf("0\n");
    }
    
    return 0;
}
