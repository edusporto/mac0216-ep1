CC=gcc
CFLAGS=-m32 -Wall
AFLAGS=-Wall -f elf32
AS=nasm

all: build

build: sieve sieve_asm sieve_nostdlib

sieve: sieve.c
	$(CC) $(CFLAGS) sieve.c -o sieve

sieve_asm: sieve_asm.c is_prime.o
	$(CC) $(CFLAGS) is_prime.o sieve_asm.c -o sieve_asm

is_prime.o: is_prime.s
	$(AS) $(AFLAGS) is_prime.s -o is_prime.o

sieve_nostdlib: is_prime.o print.o _start.o
	$(CC) $(CFLAGS) -nostdlib is_prime.o print.o _start.o sieve_nostdlib.c -o sieve_nostdlib

print.o: print.s
	$(AS) $(AFLAGS) print.s -o print.o

_start.o: _start.s
	$(AS) $(AFLAGS) _start.s -o _start.o

clean:
	rm -f sieve sieve_asm sieve_nostdlib is_prime.o print.o _start.o