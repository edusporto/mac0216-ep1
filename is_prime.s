GLOBAL is_prime

SECTION .text

is_prime:
    ; prólogo padrão
	push ebp         ; manutenção do ponteiro de base
    mov ebp, esp     ; ponto de referência para acessar parâmetros
    ; sub esp, 4     ; variável local de 4 bytes
                     ; não usarei variável local
    push ebx
    push ecx
    push edx

    ; corpo da função
    mov ecx, [ebp+8] ; guarda parâmetro o único parâmetro (n) em ecx

    ; if (n == 1) return 0;
    cmp ecx, 1
    jne testou_1
    mov eax, 0
    jmp fim

    testou_1:

    ; if (n == 2) return 1;
    cmp ecx, 2
    jne testou_2
    mov eax, 1
    jmp fim

    testou_2:

    ; if (n % 2 == 0) return 0;
    mov edx, ecx     ; salva ecx em edx
    shr ecx, 1       ; bit shift pra direita, se tiver carry, não é div 2
    jc  testou_mod2
    mov eax, 0
    jmp fim

    testou_mod2:
    mov ecx, edx     ; recupera ecx

    mov ebx, 3       ; i = 3;

    L1:              ; loop
    mov eax, ebx     ; eax = i (ebx)
    mul eax          ; eax = eax * eax
    cmp eax, ecx     ; cmp i*i, n
    jg  eh_primo     ; jump if (i*i > n)

    mov eax, ecx     ; eax = n (ecx)
    ;xor edx, edx     ; edx = 0 para guardar o mod da divisao
    div ebx          ; eax = eax / ebx , edx = eax % ebx

    cmp edx, 0
    je  n_eh_primo   ; jump if (n % i == 0)

    add ebx, 2       ; i += 2
    jmp L1

    n_eh_primo:
        mov eax, 0
        jmp fim

    eh_primo:
        mov eax, 1

    fim:
        ; epílogo padrão
        pop edx
        pop ecx
        pop ebx          ; restaurar valor original de ebx
        mov esp, ebp     ; desalocar variaveis locais
        pop ebp

        ret
