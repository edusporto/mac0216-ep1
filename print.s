GLOBAL print_asm

SECTION .text

print_asm:
    ; prólogo
    push ebp             ; manutenção do ponteiro de base
    mov ebp, esp         ; ponto de referência para acessar parâmetros
    push eax
    push ebx
    push ecx
    push edx


    mov edx, [ebp+8]     ; tamanho da string
    mov ecx, [ebp+12]    ; ponteiro para o início da string
    mov ebx, 1           ; representa stdout
    mov eax, 4           ; system call de sys_write
    int 0x80             ; chama o kernel

    fim:
        ; epílogo padrão
        pop edx
        pop ecx
        pop ebx
        pop eax
        mov esp, ebp     ; desalocar variaveis locais
        pop ebp

    ret