#include <stdio.h>
#include <stdlib.h>

int is_prime(int n);

int main(int argc, char **argv) {
    if (argc != 2) {
        // não colocou primo ou
        // colocou mais que 1 parametro
        printf("Usage: %s <NUMBER>\n", argv[0]);

        return 1;
    }

    if (is_prime(atoi(argv[1]))) {
        printf("1\n");
    } else {
        printf("0\n");
    }
    
    return 0;
}

int is_prime(int n) {
    if (n == 1) return 0;
    if (n == 2) return 1;

    if (n % 2 == 0) return 0;

    for (int i = 3; i*i <= n; i += 2) {
        // se i é divisor de n, n/i tambem é

        // o que significa que só precisamos verificar
        // i até a raiz quadrada de n

        if (n % i == 0) {
            return 0;
        } 
    }

    return 1;
}