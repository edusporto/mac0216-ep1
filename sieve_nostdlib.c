extern void print_asm(int length, char *string);
extern int is_prime(int n);

int atoi(char *str) {
    int i = -1;
    int ret = 0;
    int mul = 1;

    for (char *curr = str; *curr != '\0'; curr++) {
        // find size of string
        i++;
    }

    for (; i >= 0; i--) {
        ret += (str[i] - '0') * mul;
        mul *= 10;
    }

    return ret;
}

int main(int argc, char **argv) {
    if (argc != 2) {
        // não colocou primo ou
        // colocou mais que 1 parametro
        print_asm(33, "Usage: ./sieve_nostdlib <NUMBER>\n");

        return 1;
    }

    if (is_prime(atoi(argv[1]))) {
        print_asm(2, "1\n");
    } else {
        print_asm(2, "0\n");
    }
    
    return 0;
}
