# EP1 de MAC0216 - Técnicas de Programação I

## Aluno
Eduardo Sandalo Porto

## Pré-requisitos

* `gcc`
* `nasm`

## Compilando

* `make` — Compila `sieve`, `sieve_asm` e `sieve_nostdlib`
* `make build` — Mesmo que `make`
* `make sieve`
* `make sieve_asm`
* `make sieve_nostdlib`
* `make clean` — Remove todos os arquivos compilados e/ou montados
